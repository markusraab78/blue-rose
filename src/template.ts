import { emptyArcana } from "./module/data/Item/ArcanaData";
import { emptyArmor } from "./module/data/Item/ArmorData";
import { emptyFocus } from "./module/data/Item/FocusData";
import { emptyPhysicalItem } from "./module/data/Item/PhysicalItemData";
import { emptyPlayerCharacter } from "./module/data/Actor/PlayerCharacterData";
import { emptyWeapon } from "./module/data/Item/WeaponData";
import { emptyShield } from "./module/data/Item/ShieldData";
import { emptyTalent } from "./module/data/Item/TalentData";
import {
    physicalItemType,
    weaponType,
    shieldType,
    armorType,
    focusType,
    arcanaType,
    talentType,
} from "./module/data/Item/ItemTypes";

export const Actors = {
    pc: emptyPlayerCharacter,
};

export const Items = {
    [physicalItemType]: emptyPhysicalItem,
    [weaponType]: emptyWeapon,
    [shieldType]: emptyShield,
    [armorType]: emptyArmor,
    [focusType]: emptyFocus,
    [arcanaType]: emptyArcana,
    [talentType]: emptyTalent,
};
