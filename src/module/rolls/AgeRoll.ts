import { PlayerCharacterActor } from "../actors/PlayerCharacterActor";
import { AbilityScore } from "../data/Actor/shared/Abilities";
import { FocusData } from "../data/Item/FocusData";
import { focusType } from "../data/Item/ItemTypes";

export class AgeRoll extends Roll {
    name: string;
    modifier: number;

    static calculateFocus(focusItem: Item<FocusData>) {
        if (focusItem != undefined) {
            return focusItem.data.data.improved ? 3 : 2;
        } else {
            return 0;
        }
    }

    constructor(target: PlayerCharacterActor, ability: AbilityScore, focus: string) {
        const focusItem: Item<FocusData> = target.items.find(
            (i: Item<FocusData>) => i.type == focusType && i.name == focus
        );
        const focusBonus = AgeRoll.calculateFocus(focusItem);
        const abilityValue = target.data.data.abilities[ability];
        const modifier = abilityValue + focusBonus;
        super(`3d6+${modifier}`);
        this.modifier = modifier;
        this.name = `${ability}${focus != undefined ? ` (${focus})` : ""}`;
    }

    async render(chatOptions: any = {}) {
        chatOptions = mergeObject(
            {
                user: game.user._id,
                flavor: null,
                template: "systems/blue-rose/templates/chat/ageRoll.hbs",
            },
            chatOptions || {}
        );

        if (!this._rolled) {
            this.roll();
        }
        var rolls = this.dice[0].results.map((r) => r.result);
        var match = new Set(rolls).size !== 3;

        const chatData = {
            user: chatOptions.user,
            rolls: rolls.reduce((p, c, i) => ({ ...p, [`d${i}`]: c }), new Map<string, number>()),
            name: this.name,
            modifier:
                this.modifier == 0
                    ? undefined
                    : this.modifier > 0
                    ? `+ ${this.modifier}`
                    : `- ${Math.abs(this.modifier)}`,
            total: this.total,
            stuntPoints: match ? rolls[2] : 0,
        };

        return renderTemplate(chatOptions.template, chatData);
    }
    async toMessage(chatData) {
        chatData.content = await this.render({ user: chatData.user });
        return ChatMessage.create(chatData);
    }
}
