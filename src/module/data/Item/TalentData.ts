import { TalentType, talentType } from "./ItemTypes";
import { BaseItemData } from "./BaseItemData";

export enum TalentRank {
    Novice = 1,
    Journeyman = 2,
    Master = 3,
}

export type TalentData = BaseItemData & {
    type: TalentType;
    rank: TalentRank;
};

export const emptyTalent: TalentData = {
    type: talentType,
    rank: TalentRank.Novice,
    description: "",
};
