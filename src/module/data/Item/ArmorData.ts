import { PhysicalItemData, emptyPhysicalItem } from "./PhysicalItemData";
import { armorType, ArmorType } from "./ItemTypes";

export type ArmorData = PhysicalItemData & {
    type: ArmorType;
    armorRating: number;
    armorPenalty: number;
    equipped: boolean;
};

export const emptyArmor: ArmorData = {
    ...emptyPhysicalItem,
    type: armorType,
    description: "",
    armorPenalty: 0,
    armorRating: 0,
    equipped: false,
};
