import { FocusType, focusType } from "./ItemTypes";
import { BaseItemData } from "./BaseItemData";
import { RollableItemData } from "./RollableItemData";

export type FocusData = BaseItemData &
    RollableItemData & {
        type: FocusType;
        improved: boolean;
    };

export const emptyFocus: FocusData = {
    type: focusType,
    description: "",
    improved: false,
    defaultAbility: "",
};
