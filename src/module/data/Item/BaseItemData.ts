import { ItemType } from "./ItemTypes";

export type BaseItemData = {
    type: ItemType;
    description: string;
};
