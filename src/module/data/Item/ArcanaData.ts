import { ArcanaType, arcanaType } from "./ItemTypes";
import { BaseItemData } from "./BaseItemData";
import { ActionType } from "../shared/ActionType";
import { TestType } from "../shared/TestType";
import { AbilityScore } from "../Actor/shared/Abilities";

export type ArcanaData = BaseItemData & {
    type: ArcanaType;
    time: ActionType;
    origin: string;
    ability: AbilityScore;
    resistance: TestType;
    fatigue: string;
};

export const emptyArcana: ArcanaData = {
    type: arcanaType,
    description: "",
    time: ActionType.varies,
    origin: "",
    ability: "Accuracy",
    resistance: { ability: "", focus: "" },
    fatigue: "",
};
