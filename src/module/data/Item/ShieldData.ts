import { PhysicalItemData, emptyPhysicalItem } from "./PhysicalItemData";
import { ShieldType, shieldType } from "./ItemTypes";

export type ShieldData = PhysicalItemData & {
    type: ShieldType;
    defenseBonus: number;
    equipped: boolean;
};

export const emptyShield: ShieldData = {
    ...emptyPhysicalItem,
    type: shieldType,
    defenseBonus: 0,
    description: "",
    weight: 0,
    quantity: 1,
    equipped: false,
};
