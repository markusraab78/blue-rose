export type Abilities = {
    [k: string]: number;
    Accuracy: number;
    Communication: number;
    Constitution: number;
    Dexterity: number;
    Fighting: number;
    Intelligence: number;
    Perception: number;
    Strength: number;
    Willpower: number;
};

export type AbilityScore = keyof Abilities;

export const defaultAbilities: Abilities = {
    Accuracy: 0,
    Communication: 0,
    Constitution: 0,
    Dexterity: 0,
    Fighting: 0,
    Intelligence: 0,
    Perception: 0,
    Strength: 0,
    Willpower: 0,
};
