export type Resource = {
    value: number;
    min: number;
    max: number;
};
