export type Persona = {
    calling: string;
    destiny: string;
    fate: string;
    corruption: string;
    goals: string[];
};

export const defaultPersona: Persona = {
    calling: "",
    destiny: "",
    fate: "",
    corruption: "",
    goals: ["", "", ""],
};
