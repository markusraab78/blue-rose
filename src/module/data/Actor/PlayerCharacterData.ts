import { Abilities, defaultAbilities } from "./shared/Abilities";
import { Fatigue } from "./shared/Fatigue";
import { Persona, defaultPersona } from "./shared/Persona";
import { Relationship } from "./shared/Relationship";
import { Resource } from "./shared/Resource";

export type PlayerCharacterData = {
    race: string;
    background: string;
    class: string;

    abilities: Abilities;

    health: Resource;
    conviction: number;
    fatigue: Fatigue;

    speed: number;
    defense: number;
    armorRating: number;
    armorPenalty: number;

    persona: Persona;
    relationships: Map<number, Relationship>;
    notes: string;
};

export const emptyPlayerCharacter: PlayerCharacterData = {
    race: "",
    background: "",
    class: "",

    abilities: defaultAbilities,

    health: { max: 20, value: 20, min: 0 },
    conviction: 0,
    fatigue: Fatigue.None,

    speed: 10,
    defense: 10,
    armorRating: 0,
    armorPenalty: 0,

    persona: defaultPersona,
    relationships: new Map(),
    notes: "",
};
