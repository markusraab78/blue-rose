export enum ActionType {
    none = 0,
    minor = 1,
    major = 2,
    concentration = 3,
    varies = 4,
}
